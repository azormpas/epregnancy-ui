export class User {
   name?:string;
   surname?: string;
   residenceTown?: string;
   email?: string;
   date?:string;
   password?:string;
   newPassword?:string;
   photo?:any;
} 