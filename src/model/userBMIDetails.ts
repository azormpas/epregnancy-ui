export class UserBMIDetails {
    id?:number;
    height?:number;
    weightBefore?:number;
    currentWeight?: number;
    week?: number;
    twins?: boolean = false;
    bmi?:number;
 } 