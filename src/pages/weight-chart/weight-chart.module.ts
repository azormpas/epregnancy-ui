import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightChart } from './weight-chart';



@NgModule({
  declarations: [WeightChart],
  imports: [
   IonicPageModule.forChild(WeightChart),
  // TranslateModule.forChild()
  ]
})

export class WeightChartModule { }