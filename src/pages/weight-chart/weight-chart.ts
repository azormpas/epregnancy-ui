
import { Component, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { NavController, LoadingController, IonicPage, } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { SuccessUtil } from '../../services/success.util';
import { WeightChartService } from '../../services/weight-chart.service';
import { Chart } from 'chart.js';
import { UserBMIDetails } from '../../model/userBMIDetails';
import { ErrorUtil } from '../../services/error.util';

@IonicPage({
    name: "weight-chart",
    segment: "weight-chart"
    // defaultHistory: ["home", "profile"]
})


@Component({
    selector: 'weight-chart',
    templateUrl: 'weight-chart.html'
})
export class WeightChart {

    @ViewChild('weightChartCanvas') weightChart;

    private userDetails = new UserBMIDetails();
    private weekList: any;
    private minWeight: Array<any> = [];
    private maxWeight: Array<any> = [];
    private currentWeight: Array<any> = [];

    constructor(public navCtrl: NavController, private loading: LoadingController,
        private toastCtrl: ToastController, private successUtil: SuccessUtil,private errorUtil: ErrorUtil,
         private weightChartService: WeightChartService) {

    }

    ngOnInit() {
        this.getWeekList();
        this.chartDrawing();
    }

    ionViewDidEnter() {
        this.getBmiWhenPageLoaded();
    }


    chartDrawing() {
        var k;
        this.weightChart = new Chart(this.weightChart.nativeElement, {

            type: 'line',
            data: {
                labels: ["1η", "2η", "3η", "4η", "5η", "6η", "7η", "8η", "9η", "10η", "11η", "12η", "13η", "14η", "15η", "16η", "17η", "18η", "19η", "20η", "21η", "22η", "23η", "24η", "25η", "26η", "27η", "28η", "29η", "30η", "31η", "32η", "33η", "34η", "35η", "36η", "37η", "38η", "39η", "40η"],
                datasets: [
                    {
                        label: "Το βάρος μου",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderWidth: 3,
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 7,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 2,
                        pointHitRadius: 10,
                        data: this.currentWeight,
                        spanGaps: false,
                    },
                    {
                        label: "Όρια ιδανικού βάρους",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "#C7002E",
                        borderColor: "#C7002E",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderWidth: 3,
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#C7002E",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 7,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#C7002E",
                        pointHoverBorderColor: "#C7002E",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: this.maxWeight,
                        spanGaps: false
                    },
                    {
                        label: "empty",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "#C7002E",
                        borderColor: "#C7002E",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderWidth: 3,
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#C7002E",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 7,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: this.minWeight,
                        spanGaps: false
                    }
                ]

            },
            options: {
                legend: {
                    labels: {
                        filter: (label) => {
                            if (label.text != 'empty') return true;
                        }
                    }
                }
            }

        });
    }

    /**
     * @function setChartDrawing is used for updating three chart values,the current user's weight and the max/min weight limits.
     * @param currentWeight The current user's weight.
     * @param maxLimit List with 40 records that includes the maximum weight that user could gain.
     * @param minLimit List with 40 records that includes the minimum weight that user could gain.
     */
    setChartDrawing(currentWeight, maxLimit, minLimit) {
        this.weightChart.data.datasets[0].data = currentWeight;
        this.weightChart.data.datasets[1].data = maxLimit;
        this.weightChart.data.datasets[2].data = minLimit;
    }

    onGetWeekListSuccess = (response) => {
        this.weekList = response;
    }

    getWeekList() {
        this.weightChartService.getWeekList(this.onGetWeekListSuccess, null);
    }


    onSaveUserBMISuccess = (callback) => {
        return response => {
            callback();
            this.bmiServerResponseUtil(response);
        }
    }

    onSaveUserBMIError = (callback) => {
        return response => {
            callback();
            this.errorUtil.showError(response.code);
        }
    }
    saveUserBMI(userDetails) {
        
        //if(this.bmiPostValidation(userDetails)){
            let loading = this.loading.create({
                spinner: 'crescent',
                content: 'Παρακαλώ περιμένετε ...'
            });
    
            loading.present();
            this.weightChartService.saveUserBMI(this.bmiCalculator(userDetails), this.onSaveUserBMISuccess(() => loading.dismiss()), 
            this.onSaveUserBMIError(() => loading.dismiss()));
      //  }
       // else{
      //      this.showError("Παρακαλώ συμπληρώστε όλα τα απαιτούμενα πεδία");
      //  }
        
    }

    /**
     * @function bmiCalculator  is used to calculate the BMI of the user
     * @param {WeightLimit} userDetails contains the whole information which is releated to the user's weight chart (height,weight,twins(yes/no),week).
     */
    bmiCalculator(userDetails) {
        userDetails.bmi = userDetails.weightBefore / (this.getDigit(userDetails.height) * this.getDigit(userDetails.height));
        return userDetails;
    }


    /**
     * @function getDigit is used to convert the height from cm to meters
     * @param {number} number contains the user's height
     * @returns height to meters format
     */
    getDigit(number) {
        return number / 100;
    }

    onGetBMISuccess = (callback) => {
        return response => {
            callback();
            this.bmiServerResponseUtil(response);
        }
    }

    /**
     * @function getBmiWhenPageLoaded get the user's details (height,bmi,current weight,weight limit list,week etc)
     * when the page is loaded
     */
    getBmiWhenPageLoaded() {
        let loading = this.loading.create({
            spinner: 'crescent',
            content: 'Παρακαλώ περιμένετε ...'
        });

        loading.present();
        this.weightChartService.getUserBMI(this.onGetBMISuccess(() => {
            loading.dismiss();
        }), null);
    }

    /**
     * @param {object} response It contains the user details which is releated to the user's weight before/after pregnancy,
     * the user's bmi and a list which contains the max and min weight limits for this user
     */
    bmiServerResponseUtil(response) {
        if (response.id !=null) {
            this.minWeight.length = 0;
            this.maxWeight.length = 0;
            this.currentWeight.length = 0;
            this.userDetails = response;
            response.weightLimit.filter((o, index) => {
                this.minWeight.push(o.min + this.userDetails.weightBefore);
                this.maxWeight.push(o.max + this.userDetails.weightBefore);
                //index + 1 επειδη ψάχνουμε να βρουμε την τρέχουσα εβδομάδα εγκυμοσύνης.Οι εβδομάδες ξεκινάν απο 1 - 40 και το index απο 0-39 
                (index + 1 != this.userDetails.week) ? this.currentWeight.push(null) : this.currentWeight.push(this.userDetails.currentWeight);
            });

            // this.setChartDrawing(this.currentWeight, this.maxWeight, this.minWeight);
            //update the chart getting the new values
            this.weightChart.update();
        }
    }


    bmiPostValidation(userDetails):boolean{
        
        if(userDetails.height && userDetails.weightBefore && userDetails.currentWeight && userDetails.week){
            return true;
        }
        else{
            return false;
        }
    }        

    showError(message) {
     
                 const toast = this.toastCtrl.create({
                    message: message,
                    duration: 4000,
                    position: "top"
                });
                toast.present();
        }

}
