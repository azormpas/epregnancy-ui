import { Component } from '@angular/core';
import { NavController, IonicPage, LoadingController } from 'ionic-angular';
import { FormControl, FormBuilder, FormGroup } from "@angular/forms";


@IonicPage({
    name: "create-new-post",
    segment: "create-new-post"
   // defaultHistory: ["home", "profile"]
  })

@Component({
  selector: 'create-new-post',
  templateUrl: 'create-new-post.html'
})
export class CreateNewPost {

  item: FormControl;
  private imageArray:Array<any> = [];
  private size:number = 2;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder,private loadingCtrl:LoadingController) {

  }
  


  ionViewWillLoad() {
    this.item = this.formBuilder.control('');
  }

  uploadImage(e) {
    let reader = new FileReader();
    if (e.target.files[0] != null) {
      reader.readAsDataURL(e.target.files[0]);      
      this.size = Number((this.size - (e.target.files[0].size/1000000)).toFixed(2));
    }

    reader.onload = (event: any) => {
      const img = new Image();
      img.src = event.target.result;  

      const loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Παρακαλώ περιμένετε ...'
      });

      loading.present().then(()=>{
  
        const newHeight = this.getThumbnailHeight(img.height,img.width);

        const image = {
          id:Math.random().toString(),
          src:img.src,
          height:newHeight,
          width:100,
          size:e.target.files[0].size
        }
        this.imageArray.push(image);
        
        loading.dismiss();
     });
   
    }
  }

getThumbnailHeight(height,width){
return (height / width) * 100;
}

removeImage(id){
  const img = this.imageArray.find(img=>img.id == id);
  this.size = Number((this.size + (img.size/1000000)).toFixed(2));
  this.imageArray = this.imageArray.filter(image => image.id !== id);
}

openImage(id) {

    var modal = document.getElementById('forumModal') as HTMLElement;

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById(id) as HTMLImageElement;
    var modalImg = document.getElementById("forumImage") as HTMLImageElement;

    modal.style.display = "block";
    modalImg.src = img.src;

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0] as HTMLElement;

    // When the user clicks on <span> (x), close the modal
    span.onclick = () => {
      modal.style.display = "none";
    }
  }

}