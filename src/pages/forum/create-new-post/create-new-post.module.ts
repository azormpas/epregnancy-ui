import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateNewPost } from './create-new-post';
import { TextEditorModule } from '../text-editor/text-editor.module';



@NgModule({
  declarations: [CreateNewPost],
  imports: [
   IonicPageModule.forChild(CreateNewPost),
   TextEditorModule
  // TranslateModule.forChild()
  ]
})

export class CreateNewPostModule { }