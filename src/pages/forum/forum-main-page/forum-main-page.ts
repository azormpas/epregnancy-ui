import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, IonicPage, } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { SuccessUtil } from '../../../services/success.util';
import { ErrorUtil } from '../../../services/error.util';

@IonicPage({
  name: "forum-main-page",
  segment: "forum-main-page"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector: 'forum-main-page',
  templateUrl: 'forum-main-page.html'
})
export class ForumMainPage {


  private items:Array<any>=[];
   private photo:string;
    
   constructor(public navCtrl: NavController, private loading: LoadingController,
    private toastCtrl: ToastController, private successUtil: SuccessUtil,private errorUtil: ErrorUtil) {

    }
 

    goToForumSectionPage(){
      this.navCtrl.push("forum-section-page");
    }

   goToCreteNewPostPage(){
     this.navCtrl.push("create-new-post");
   }

}
