import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForumMainPage } from './forum-main-page';



@NgModule({
  declarations: [ForumMainPage],
  imports: [
   IonicPageModule.forChild(ForumMainPage),
  // TranslateModule.forChild()
  ]
})

export class ForumMainPageModule { }