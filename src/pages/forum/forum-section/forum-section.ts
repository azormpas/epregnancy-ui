
import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, IonicPage, } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { SuccessUtil } from '../../../services/success.util';
import { ErrorUtil } from '../../../services/error.util';

@IonicPage({
  name: "forum-section-page",
  segment: "forum-section-page"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector: 'forum-section',
  templateUrl: 'forum-section.html'
})
export class ForumSectionPage {


  private items:Array<any>=[];
   private photo:string;
    
   constructor(public navCtrl: NavController, private loading: LoadingController,
    private toastCtrl: ToastController, private successUtil: SuccessUtil,private errorUtil: ErrorUtil) {
      for (let i = 0; i < 30; i++) {
        this.items.push( this.items.length );
      }
    }
  
    doInfinite(infiniteScroll) {
      console.log('Begin async operation');
  
      setTimeout(() => {
        for (let i = 0; i < 30; i++) {
          this.items.push( this.items.length );
        }
  
        console.log('Async operation has ended');
        infiniteScroll.complete();
      }, 500);
    }

    goToUserPost(){
      this.navCtrl.push('forum-user-post');
    }


    goToCreteNewPostPage(){
      this.navCtrl.push("create-new-post");
    }

}
