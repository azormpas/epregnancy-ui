import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForumSectionPage } from './forum-section';



@NgModule({
  declarations: [ForumSectionPage],
  imports: [
   IonicPageModule.forChild(ForumSectionPage),
  // TranslateModule.forChild()
  ]
})

export class ForumSectionPageModule { }