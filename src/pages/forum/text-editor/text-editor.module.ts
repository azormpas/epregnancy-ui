import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TextEditor } from './text-editor';

@NgModule({
  declarations: [
    TextEditor,
  ],
  imports: [
    IonicPageModule.forChild(TextEditor),
  ],
  exports: [
    TextEditor
  ]
})
export class TextEditorModule {}