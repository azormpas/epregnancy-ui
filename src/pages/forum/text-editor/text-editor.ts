import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { FormControl } from "@angular/forms";
import { IonicPage } from 'ionic-angular';


@IonicPage({
    name: "text-editor",
    segment: "text-editor"
  })


@Component({
  selector: 'text-editor',
  templateUrl: 'text-editor.html'
})
export class TextEditor {

  @ViewChild('editor') editor: ElementRef;
  @ViewChild('decorate') decorate: ElementRef;

  @Input() formControlItem: FormControl;

  @Input() placeholderText: string;



  constructor() {

  }



ngOnInit(): void {
  this.wireupButtons();
  this.updateItem();
}

  getPlaceholderText() {
    if (this.placeholderText !== undefined) {
      return this.placeholderText
    }
    return '';
  }

  uniqueId = `editor${Math.floor(Math.random() * 1000000)}`;

  private stringTools = {
    isNullOrWhiteSpace: (value: string) => {
      if (value == null || value == undefined) {
        return true;
      }
      value = value.replace(/[\n\r]/g, '');
      value = value.split(' ').join('');

      return value.length === 0;
    }
  };

  private updateItem() {
    let element:any = this.editor.nativeElement as HTMLDivElement;
    element.innerHTML = this.formControlItem.value;

    // if (element.innerHTML === null || element.innerHTML === '') {
    //   element.innerHTML = '<div></div>';
    // }
    const reactToChangeEvent = () => {

      if (this.stringTools.isNullOrWhiteSpace(element.innerText)) {
        element.innerHTML = '<div></div>';
        this.formControlItem.setValue(null);
      } else {
        this.formControlItem.setValue(element.innerHTML);
      }
    };

    element.onchange = () => reactToChangeEvent();
    element.onkeyup = () => reactToChangeEvent();
    element.onpaste = () => reactToChangeEvent();
    element.oninput = () => reactToChangeEvent();
  }

  private wireupButtons() {
    console.log("#####   wireupButtons()")
    let buttons = (this.decorate.nativeElement as HTMLDivElement).getElementsByTagName('button');
    for (let i = 0; i < buttons.length; i++) {
      let button = buttons[i];

      let command = button.getAttribute('data-command');

      if (command.includes('|')) {
        let parameter = command.split('|')[1];
        command = command.split('|')[0];

        button.addEventListener('click', () => {
          document.execCommand(command, false, parameter);
        });
      } else {
        button.addEventListener('click', () => {
          document.execCommand(command);
        });
      }
    }

  }


  getSelectedButton(id){
    const element = document.getElementById(id);
    console.log(element.className);
    if(element.className != "selectedButton activated"){
      console.log("selectedButton");
      element.className = "selectedButton";      
      this.editor.nativeElement.focus();
    } else {
      console.log("unselectedButton");
      element.className = "unselectedButton";
    }
  }


}