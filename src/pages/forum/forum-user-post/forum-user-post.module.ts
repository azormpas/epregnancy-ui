import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForumUserPostPage } from './forum-user-post';



@NgModule({
  declarations: [ForumUserPostPage],
  imports: [
   IonicPageModule.forChild(ForumUserPostPage),
  // TranslateModule.forChild()
  ]
})

export class ForumUserPostPageModule { }