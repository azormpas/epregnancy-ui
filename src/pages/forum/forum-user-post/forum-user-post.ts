
import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, IonicPage, } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { SuccessUtil } from '../../../services/success.util';
import { ErrorUtil } from '../../../services/error.util';

@IonicPage({
  name: "forum-user-post",
  segment: "forum-user-post"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector: 'forum-user-post',
  templateUrl: 'forum-user-post.html'
})
export class ForumUserPostPage {


  private items:Array<any>=[];
   private photo:string;
    
   constructor(public navCtrl: NavController, private loading: LoadingController,
    private toastCtrl: ToastController, private successUtil: SuccessUtil,private errorUtil: ErrorUtil) {
      for (let i = 0; i < 30; i++) {
        this.items.push( this.items.length );
      }
    }
  
    doInfinite(infiniteScroll) {
      console.log('Begin async operation');
  
      setTimeout(() => {
        for (let i = 0; i < 30; i++) {
          this.items.push( this.items.length );
        }
  
        console.log('Async operation has ended');
        infiniteScroll.complete();
      }, 500);
    }

}
