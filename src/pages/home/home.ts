
import { Component, OnInit } from '@angular/core';
import { NavController, IonicPage, LoadingController } from 'ionic-angular';
import { JwtHelper, AuthHttp } from "angular2-jwt";



import { User } from './../../model/user';
import { TestList } from "../testList/testList";
import { AuthProvider } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { HomeService } from '../../services/home.service';

@IonicPage({
  name: "home",
  segment: "home"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  email: string;
  token: string;
  date: string;
  weeks: number;
  dateFormat: string;
  dateValue: boolean = false;
  user = new User();

  private dateForm: boolean = false;
  private validDate: boolean = false;
  private errorMessage: boolean = false;
  private spinner: boolean;
  private currentDate:string;
  private nineMonthBeforeDate:string;


  constructor(public navCtrl: NavController,
    private readonly authProvider: AuthProvider,
    public jwtHelper: JwtHelper,
    private readonly authHttp: AuthHttp,
    private userService: UserService,
    private readonly loadingCtrl: LoadingController,
    private homeService : HomeService
  ) { }

  ngOnInit() {
    this.authentication();
  
  }
  ionViewDidEnter() {
    this.getValidConceptualDate();
    this.checkDate();
    this.spinner = true;

  }

  onCheckDateSuccess = (response) =>{
    if (response.message == "NOT EXIST")
    {
      this.dateForm = true;
    }
    else {

      this.spinner = true;
      this.dateFormat = response.date;
      this.weeks = response.weeks;
      var birthDay = this.dateFormat.split("-").pop();
      var birthMonth = this.dateFormat.split("-")[1];
      this.dateFormat = response.date.split("-")[2] +"/"+response.date.split("-")[1] + "/"+ response.date.split("-")[0];
      this.dateValue = true;
      this.spinnerInitialize(this.birthMonthCalculation(birthMonth), this.birthDayCalculation(birthDay, birthMonth));

  }
}

  checkDate() {
    this.homeService.checkDate(this.onCheckDateSuccess,null);
  }

  onGetValidConceptualDateSuccess = (response) => {
    this.currentDate = response.currentDate;
    this.nineMonthBeforeDate = response.nineMonthsAgo;
}


  getValidConceptualDate(){
    this.homeService.getValidConceptualDate(this.onGetValidConceptualDateSuccess,null);
  }

  onSaveConceptualDate = (response) =>{

      this.errorMessage = false;
      this.dateForm = false;
      this.spinner = true;

      let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Logging in ...'
      });

      loading.present().then(() => {
        this.checkDate();
        loading.dismiss();
      })
    
  }

  saveDate(date) {
    if (date == null) {
      this.validDate = true;
    }
    else {
      var req = {
        date: date,
        email: this.email
      }
      this.validDate = false;
      this.homeService.saveConceptualDate(req, this.onSaveConceptualDate, null);
    }
  }

  getGynecologicalTestListPage(){

    //get testListResult and navigate to TestList Page
    this.navCtrl.push('testlist', {
      testList: this.weeks.toString(),
    })


  }


  authentication() {
    this.authProvider.authUser.subscribe(jwt => {
      if (jwt) {
        this.token = jwt;
        const decoded = this.jwtHelper.decodeToken(jwt);
        this.email = decoded.sub;
        this.user.email = this.email;
      }
      else {
        this.email = null;
      }
    });
  }

  logout() {
    this.authProvider.logout();
  }


  birthDayCalculation(birthDay, birthMonth) {
    var birthDayCalculationDay;

    if (birthDay == "01") {
      birthDayCalculationDay = 0.25;
    }
    if (birthDay == "02") {
      birthDayCalculationDay = 0.24;
    }
    if (birthDay == "03") {
      birthDayCalculationDay = 0.23;

    }
    if (birthDay == "04") {
      birthDayCalculationDay = 0.22;
    }
    if (birthDay == "05") {
      birthDayCalculationDay = 0.2;

    }
    if (birthDay == "06") {
      birthDayCalculationDay = 0.19;
    }
    if (birthDay == "07") {
      birthDayCalculationDay = 0.18;
    }
    if (birthDay == "08") {
      birthDayCalculationDay = 0.17;
    }
    if (birthDay == "09") {
      birthDayCalculationDay = 0.16;
    }
    if (birthDay == "10") {
      birthDayCalculationDay = 0.14;
    }
    if (birthDay == "11") {
      birthDayCalculationDay = 0.12;
    }
    if (birthDay == "12") {
      birthDayCalculationDay = 0.10;
    }

    if (birthDay == "13") {
      birthDayCalculationDay = 0.09;
    }
    if (birthDay == "14") {
      birthDayCalculationDay = 0.08;
    }
    if (birthDay == "15") {
      birthDayCalculationDay = 0.05;
    }
    if (birthDay == "16") {
      birthDayCalculationDay = 0.03;
    }
    if (birthDay == "17") {
      birthDayCalculationDay = 0.02;

    }
    if (birthDay == "18") {
      birthDayCalculationDay = 0;
    }
    if (birthDay == "19") {
      birthDayCalculationDay = -0.02;
    }
    if (birthDay == "20") {
      birthDayCalculationDay = -0.04;
    }
    if (birthDay == "21") {
      birthDayCalculationDay = -0.05;
    }
    if (birthDay == "22") {
      birthDayCalculationDay = -0.07;
    }
    if (birthDay == "23") {
      birthDayCalculationDay = -0.082;
    }
    if (birthDay == "24") {
      birthDayCalculationDay = -0.1;
    }

    if (birthDay == "25") {
      birthDayCalculationDay = -0.13;
    }
    if (birthDay == "26") {
      birthDayCalculationDay = -0.15;
    }
    if (birthDay == "27") {
      birthDayCalculationDay = -0.165;
    }

    if (birthDay == "28" && birthMonth == 2) {
      birthDayCalculationDay = -0.215;
    }

    if (birthDay == "28") {
      birthDayCalculationDay = -0.172;
    }

    if (birthDay == "29") {
      birthDayCalculationDay = -0.18;
    }
    if (birthDay == "30") {
      birthDayCalculationDay = -0.215;
    }
    if (birthDay == "31") {
      birthDayCalculationDay = -0.222;
    }

    return birthDayCalculationDay;
  }

  birthMonthCalculation(birthMonth) {

    var birthMonthCalculationDay;

    if (birthMonth == "01") {
      birthMonthCalculationDay = 1;
    }
    if (birthMonth == "02") {
      birthMonthCalculationDay = 2;
    }
    if (birthMonth == "03") {
      birthMonthCalculationDay = 3;
      console.log('i am march')
    }
    if (birthMonth == "04") {
      birthMonthCalculationDay = 4;
    }
    if (birthMonth == "05") {
      birthMonthCalculationDay = 5;

    }
    if (birthMonth == "06") {
      birthMonthCalculationDay = 6;
    }
    if (birthMonth == "07") {
      birthMonthCalculationDay = 7;
    }
    if (birthMonth == "08") {
      birthMonthCalculationDay = 8;
    }
    if (birthMonth == "09") {
      birthMonthCalculationDay = 9;
    }
    if (birthMonth == "10") {
      birthMonthCalculationDay = 10;
    }
    if (birthMonth == "11") {
      birthMonthCalculationDay = 11;
    }
    if (birthMonth == "12") {
      birthMonthCalculationDay = 0;
    }

    return birthMonthCalculationDay;

  }

  colorCalculator(colors,colorShadowBirthMonth){
      if (colorShadowBirthMonth == 10) 
        {
              for (var i = 1; i <= 12; i++) 
                {
                  if (i == colorShadowBirthMonth + 1 || i == colorShadowBirthMonth + 2 || i == 1) 
                    {
                      colors.push("#edafbc");
                    }
                  else 
                    {
                      colors.push("#c7002e");
                    }
                  }
        }
        else if (colorShadowBirthMonth == 11) {
              for (var y = 1; y <= 12; y++) {

                  if (y == colorShadowBirthMonth + 1 || y == 1 || y == 2) 
                    {
                      colors.push("#edafbc");
                    }
                  else 
                    {
                      colors.push("#c7002e");
                    }
                  }  
          }
          else {
              for (var j = 1; j <= 12; j++) {
                  if (j == colorShadowBirthMonth + 1 || j == colorShadowBirthMonth + 2 || j == colorShadowBirthMonth + 3) {
                    colors.push("#edafbc");
                  }
                  else {
                    colors.push("#c7002e");
                  }
                }
              }
            }

  spinnerInitialize(birthMonth, birthDay) {
    var canvas: any = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");

    var colorShadowBirthMonth = parseInt(birthMonth);
    var colors = [];

    this.colorCalculator(colors,colorShadowBirthMonth);

    var numbers = ["Ιαν", "Φεβ", "Μαρ", "Απρ",
      "Μαϊος", "Ιουν", "Ιουλ", "Αυγ",
      "Σεπ", "Οκτ", "Νοε", "Δεκ"];

    var days = ["5,10,15,20,25,31", "5,10,15,20,25,28", "5,10,15,20,25,31", "5,10,15,20,25,30",
      "5,10,15,20,25,31", "5,10,15,20,25,30", "5,10,15,20,25,31", "5,10,15,20,25,31",
      "5,10,15,20,25,30", "5,10,15,20,25,31", "5,10,15,20,25,30", "5,10,15,20,25,31"];

    var symbols = ["|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|"
      , "|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|", "|,|,|,|,|,|"];

    var pocketCount = 12;
    var birthText = numbers[birthMonth];
    var cw = canvas.width = 405
    var ch = canvas.height = 425;
    var cx = cw / 2;
    var cy = ch / 2;

    // wheel & arrow are used often so cache them
    var wheelCanvas = drawPregnancyWheel();
    var arrow = drawArrow();

    var wheel = {
      cx: cw / 2,
      cy: ch / 2,
      radius: Math.min(cw, ch) / 2 - 20,
      startAngle: 0,
      endAngle: Math.PI * 4 + spin(birthText),
      totalSteps: 360,
      currentStep: 0,
    }

    drawAll(wheel);

    window.requestAnimationFrame(animate);

    function spin(hit) {
      var PI = Math.PI;
      var PI2 = PI * 2;
      var index = numbers.indexOf(birthText);
      var pocketSweep = PI2 / pocketCount;

    //give the calculated day
      var day = birthDay;

      return ((PI2 - pocketSweep * (index + 1)) + 1.5 * pocketSweep - PI / 2 + day);
    }

    function animate(time) {
      if (wheel.currentStep > wheel.totalSteps) { return; }
      drawAll(wheel);
      wheel.currentStep++;
      requestAnimationFrame(animate);
    }

    function easing(w) {
      var t = w.currentStep;
      var b = w.startAngle;
      var d = w.totalSteps;
      var c = w.endAngle - w.startAngle;
      // Penner's OutQuart
      return (-c * ((t = t / d - 1) * t * t * t - 1) + b + w.startAngle);
    }

    function drawAll(w) {
      var angle = easing(w);
      ctx.clearRect(0, 0, cw, ch);
      ctx.translate(cx, cy);
      ctx.rotate(angle);
      ctx.drawImage(wheelCanvas, -wheelCanvas.width / 2, -wheelCanvas.height / 2);
      ctx.rotate(-angle);
      ctx.translate(-cx, -cy);
      ctx.drawImage(arrow, cx - arrow.width / 2, 0);
    }

    function drawPregnancyWheel() {
      var outsideRadius = 200;
      var textRadius = 165;
      var insideRadius = 30;
      var canvas = document.createElement("canvas");
      var ctx = canvas.getContext("2d");
      canvas.width = canvas.height = outsideRadius * 2 + 6;
      var x = outsideRadius + 3;
      var y = outsideRadius + 3;
      var arc = Math.PI / (pocketCount / 2);
      ctx.strokeStyle = "white";
      ctx.font = '18px Helvetica, Arial';
      ctx.lineWidth = 4;

      // wheel

      for (var i = 0; i < pocketCount; i++) {

        var angle = i * arc;
        ctx.fillStyle = colors[i];

        ctx.beginPath();
        ctx.arc(x, y, outsideRadius, angle, angle + arc, false);
        ctx.arc(x, y, insideRadius, angle + arc, angle, true);

        //  ctx.stroke();
        ctx.fill();
        ctx.save();

        ctx.beginPath();
        ctx.arc(x, y, outsideRadius - 60, angle, angle + arc, false);
        //ctx.arc(x, y, insideRadius, angle + arc, angle, true);

        ctx.stroke();
        ctx.fill();
        ctx.save();

        ctx.shadowOffsetX = -1;
        ctx.shadowOffsetY = -1;
        ctx.shadowBlur = 0;
        ctx.shadowColor = "rgb(220,220,220)";
        ctx.fillStyle = "white";

        ctx.translate(x + Math.cos(angle + arc / 2) * textRadius,
          y + Math.sin(angle + arc / 2) * textRadius);
        ctx.rotate(angle + arc / 2 + Math.PI / 2);
        var text = numbers[i];

        ctx.fillText(text, -ctx.measureText(text).width / 2, 10);

        ctx.font = "12px Arial";
        var k = [];
        var tempSympols = [];
        k = days[i].split(",");
        tempSympols = symbols[i].split(",");
        ctx.translate(-40, 0);
        ctx.rotate(0);
        var z = 15
        var p = 0;
        var q = -8
        var yp = -1;

        for (var t = 0; t < k.length; t++) {

          if (t == 0) {
            q = -11.2;
            p = -0.1;
            yp = -2;
          }
          else if (t == 1) {
            q = -11.5
            p = 0.08;
            yp = -1;
          }
          else if (t == 2) {
            p = 0.1;
            q = -11;
            yp = -1;
          }
          else if (t == 3) {
            p = 0.1;
            q = -10
            yp = -1;
          }
          else if (t == 4) {
            p = 0.1;
            q = -9
            yp = -1;
          }
          else {
            p = 0.1;
            q = -8
            yp = -1;
          }

          ctx.fillText(tempSympols[t], -ctx.measureText(tempSympols[t]).width / 2, -20);
          ctx.fillText(k[t], -ctx.measureText(k[t]).width / 2, q);
          ctx.translate(z, yp);
          ctx.rotate(p);

          p = 0;
        }

        k.length = 0;

        ctx.restore();
      }

      return (canvas);
    }

    function drawArrow() {
      var canvas = document.createElement("canvas");
      var ctx = canvas.getContext("2d");
      canvas.width = 18;
      canvas.height = 18;
      //Arrow
      ctx.fillStyle = "yellow";
      ctx.beginPath();
      ctx.moveTo(5, 0);
      ctx.lineTo(13, 0);
      ctx.lineTo(13, 10);
      ctx.lineTo(18, 10);
      ctx.lineTo(9, 18);
      ctx.lineTo(0, 10);
      ctx.lineTo(5, 10);
      ctx.lineTo(5, 0);

      ctx.fill();
      return (canvas);
    }


  }
}
