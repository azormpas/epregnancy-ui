import { Component, ErrorHandler } from '@angular/core';
import { NavController, IonicPage, LoadingController, ToastController } from 'ionic-angular';

import { User } from './../../model/user';
import { UserService } from '../../services/user.service';
import { RegistrationService } from '../../services/registration.service';

@IonicPage({
  name: "registration",
  segment: "registration"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector: 'register-page',
  templateUrl: 'register.html'
})
export class RegisterPage {

  private successMessage: boolean = false;
  private failedMessage: boolean = false;
  private repassword: string; 
  private user = new User();


  private statusCode: number;

  constructor(public navCtrl: NavController, private registrationService: RegistrationService, 
    private loading: LoadingController, private toastCtrl: ToastController) {
  }

  getWelcomePage() {
    this.navCtrl.push('welcome');
  }

  onRegistrationSuccess =(callback) => {
    return response =>{
      callback();
    this.successMessage = true;
  } 
}


  registration(user: User) {
    let loading = this.loading.create({
      spinner: 'crescent',
      content: 'Παρακαλώ περιμένετε ...'
    });

    loading.present();

    if (this.repassword != this.user.password) {
      let message = "Οι δύο κωδικοί δεν είναι πανομοιότυποι.";
      this.handleError(message)
    }
    else{
      this.registrationService.registration(user, this.onRegistrationSuccess(() => {
        loading.dismiss();
      }), null);
    }
    
  }

    handleError(message) {
    const toast = this.toastCtrl.create({
      message,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  goToLoginPage(){
    this.navCtrl.push('welcome');
  }

  cancel(){
    this.navCtrl.push('welcome');
  }

}