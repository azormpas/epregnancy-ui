
//import { SERVER_URL } from './../../config';
import { User } from './../../model/user';
import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage, } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { AuthProvider } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { SuccessUtil } from '../../services/success.util';

import { ProfileService } from '../../services/profile.service';

@IonicPage({
  name: "profile",
  segment: "profile"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector: 'profile-page',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  private saveButton: boolean = true;
  private user = new User();
  private userBackUp = new User();
  private repassword: string = null;
  private changePasswordVisibility: boolean = false;
  private currentDate: string;
  private nineMonthBeforeDate: string;


  //private photo:any="assets/icon/placeholder-test.png";

  constructor(public navCtrl: NavController, private loading: LoadingController, private userService: UserService,
    private readonly authProvider: AuthProvider, private toastCtrl: ToastController,
    private profileService: ProfileService,private successUtil:SuccessUtil) {

     this.user.photo = "assets/icon/add-image.png";
  }

  ngOnInit() {
    // this.userBackUp = this.user;
    this.getProfileData();
     this.getValidConceptualDate();
  }




  onGetValidConceptualDateSuccess = (response) => {
    this.currentDate = response.currentDate;
    this.nineMonthBeforeDate = response.nineMonthsAgo;
}


  getValidConceptualDate(){
    this.profileService.getValidConceptualDate(this.onGetValidConceptualDateSuccess,null);
  }

  onGetProfileDataSuccess = (callback) => {
    return response =>{
      callback();
      if (response.photo == null) {
        response.photo = "assets/icon/add-image.png";
      }
      this.user = Object.assign({}, response);
      this.userBackUp = Object.assign({}, response);
    }
  }

  getProfileData() {
    let loading = this.loading.create({
      spinner: 'crescent',
      content: 'Παρακαλώ περιμένετε ...'
    });

    loading.present();
    this.profileService.getUserData({},this.onGetProfileDataSuccess(() => {
      loading.dismiss();
    }),null);
  
  }

  onUpdateSuccess = (callback) => {
    return response =>{
      callback();
      console.log("DES RESPONSE");
      console.log(response);
    // this.showMessage(response.code);
      this.successUtil.showMessage(response.code);
      this.getProfileData();
      this.saveButtonVisibility(true);
      this.initializePasswordFields();
    }
  }

  update(user) {
    let loading = this.loading.create({
      spinner: 'crescent',
      content: 'Παρακαλώ περιμένετε ...'
    });

    loading.present();

    if (user.newPassword == this.repassword) {
      if(this.user.photo == "assets/icon/add-image.png"){
        this.user.photo = null;
      }
      this.changePasswordVisibility = false;
      this.profileService.updateUserData(user, this.onUpdateSuccess(() => {
        loading.dismiss();
      }), null);
    } 
    else {
      this.showMessage("Ο κωδικοί δεν είναι ίδιοι μεταξύ τους");
    }
  }


  showMessage(text) {
    const toast = this.toastCtrl.create({
      message: text,
      duration: 4000,
      position: 'top'
    });

    toast.present();
  }

  modelChanged(): boolean {

    if (this.userBackUp.name != this.user.name) {
      return this.saveButtonVisibility(false);
    }
    else if (this.userBackUp.surname != this.user.surname) {
      return this.saveButtonVisibility(false);
    }
    else if (this.userBackUp.residenceTown != this.user.residenceTown) {
      return this.saveButtonVisibility(false);
    }
    else if (this.userBackUp.date != this.user.date) {
      return this.saveButtonVisibility(false);
    }
    // else if (this.userBackUp.email != this.user.email) {
    //   return this.saveButtonVisibility(false);
    // }
    else {
      return this.saveButton = true;
    }
  }

  passwordModelChangeValidation() {
    if ((this.user.password != null && this.user.password != "") &&
      (this.user.newPassword != null && this.user.newPassword != "") &&

      
      (this.repassword != null && this.repassword != "") || !this.modelChanged()) {
      return this.saveButtonVisibility(false);
    }
    else {
      return this.saveButton = true;
    }
  }

  saveButtonVisibility(flag): boolean {
    this.saveButton = flag;

    return flag;
  }

  logout() {
    this.authProvider.logout();
  }

  changePassword() {
    this.changePasswordVisibility = true;
  }
  //true shmainei mh energo
  //false energo
  closeChangePasswordForm() {
    this.changePasswordVisibility = false;
    (this.modelChanged()) ? this.saveButton = true : this.saveButton = false;
    this.initializePasswordFields();
  }

  initializePasswordFields() {
    this.user.password = null;
    this.user.newPassword = null;
    this.repassword = null;
  }


  uploadImage(e) {
    var reader = new FileReader();
    reader.onload = (event: any) => {
      var img = new Image();
      img.src = event.target.result;
      //this.photo = img.src ;
      //console.log(this.photo);
      this.user.photo = img.src;
      this.saveButton = false;
    }
    if (e.target.files[0] != null) {
      reader.readAsDataURL(e.target.files[0]);
    }
  }


openImage() {
    // Get the modal
    var modal = document.getElementById('myModal') as HTMLElement;

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById("myImg") as HTMLImageElement;
    var modalImg = document.getElementById("img01") as HTMLImageElement;

    modal.style.display = "block";
    modalImg.src = img.src;

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0] as HTMLElement;

    // When the user clicks on <span> (x), close the modal
    span.onclick = () => {
      modal.style.display = "none";
    }
  }


}
