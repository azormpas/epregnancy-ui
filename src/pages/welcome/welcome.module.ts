import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WelcomePage } from './welcome';
import { LoginPageModule } from '../login/login.module';




@NgModule({
  declarations: [WelcomePage],
  imports: [
   IonicPageModule.forChild(WelcomePage),
   LoginPageModule
  // TranslateModule.forChild()
  ]
})

export class WelcomePageModule { }