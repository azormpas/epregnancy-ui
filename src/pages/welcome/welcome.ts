import { Component, ViewChild } from '@angular/core';
import { NavController,IonicPage,Content } from 'ionic-angular';

@IonicPage({
  name: "welcome",
  segment: "welcome"
 // defaultHistory: ["home", "profile"]
})

@Component({
  selector: 'welcome-page',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController) {
    //this.content.contentTop =
  }

  getLoginPage(){
    this.navCtrl.push('login');
  }

   getRegisterPage(){
    this.navCtrl.push('registration');
  }

}