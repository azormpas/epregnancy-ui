import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestList } from './testList';



@NgModule({
  declarations: [TestList],
  imports: [
   IonicPageModule.forChild(TestList),
  // TranslateModule.forChild()
  ]
})

export class TestListModule { }