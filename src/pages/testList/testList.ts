import { ExaminationResult } from './../examination-result/examination-result';
import { Component,ViewChild } from '@angular/core';
import { NavController, IonicPage,Tabs,NavParams } from 'ionic-angular';

import { ProfilePage } from './../profile/profile';
import { HomePage } from './../home/home';
import { UserService } from '../../services/user.service';
import { AuthProvider } from '../../services/auth.service';
import { TestListService } from '../../services/testList.service';


@IonicPage({
  name: "testlist",
  segment: "testlist"
 // defaultHistory: ["home", "profile"]
})

@Component({
  selector:'test-list',
  templateUrl: 'testList.html'
})
export class TestList {


  private week : string ;
  private testList : Array<any> = [];

  constructor(private navCtrl : NavController,private navParam : NavParams,
  private readonly authProvider: AuthProvider,private testListService:TestListService) {
  }

  ngOnInit(){
    this.week =  this.navParam.get('testList');
    this.getGynecologicalTestList(this.week);
  }

  ngOnDestroy(){
    this.testList.length = 0;
  }

  goToExaminationResultPage(week){
    this.navCtrl.push('examination-result',{
       weekValue : week
    })
  }

  onGetGynecologicalTestListSuccess = (response) =>{
    this.testList = response;
  }
    
  getGynecologicalTestList(week){
    this.testListService.getGynecologicalTestList(week,this.onGetGynecologicalTestListSuccess,null);
  }

  logout() {
    this.authProvider.logout();
  }

}