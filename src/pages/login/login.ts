import { User } from './../../model/user';
import { Component } from '@angular/core';
import { NavController, IonicPage, LoadingController, ToastController, Tabs } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import { AuthProvider } from '../../services/auth.service';


@IonicPage({
  name: "login",
  segment: "login"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {

   private user = new User();
    
  constructor(public navCtrl: NavController,
    private readonly authProvider: AuthProvider, 
    private readonly loadingCtrl: LoadingController,
    private readonly toastCtrl: ToastController)
   {}

  submitData(){
    this.navCtrl.push('tabs');
  }

   registerPage(){
    this.navCtrl.push('registration');
  }

    login(value: any) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Παρακαλώ περιμένετε ...'
    });

    loading.present();

    this.authProvider
      .login(value)
      .finally(() => loading.dismiss())
      .subscribe(
        () => {},
        err => this.handleError(err));
  }


   handleError(error: any) {
    let message: string;
    if (error.status && error.status === 401) {
      message = 'Η σύνδεση απέτυχε';
    }
    else {
      message = `Σφάλμα: ${error.statusText}`;
    }

    const toast = this.toastCtrl.create({
      message,
      duration: 4000,
      position: 'top'
    });

    toast.present();
  }

}
