import { Component,ViewChild } from '@angular/core';
import { NavController, IonicPage,Tabs,NavParams, LoadingController } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { ProfilePage } from './../profile/profile';
import { HomePage } from './../home/home';
import { UserService } from '../../services/user.service';
import { AuthProvider } from '../../services/auth.service';
import { ExaminationResultService } from '../../services/examinationResult.service';

@IonicPage({
  name: "examination-result",
  segment: "examination-result"
 // defaultHistory: ["home", "profile"]
})

@Component({
  selector:'examination-result',
  templateUrl: 'examination-result.html'
})
export class ExaminationResult {
 
  private examinationInfo : any ;
  private weeks : string;
  private videoUrl:SafeUrl;
  private videoAvailable:boolean=false;

  constructor(private navCtrl : NavController,private navParam : NavParams,private userService: UserService,
    private sanitizer:DomSanitizer,private readonly authProvider: AuthProvider,
    private examinationResultService:ExaminationResultService,private loading: LoadingController) {
    
  }

   ionViewDidLoad(){
     this.weeks = this.navParam.get('weekValue');
     this.getGynecologicalWeekInformation(this.weeks);
  }
  
  	getSafeUrl(url) {
    if(url != null) { 
    var youtubeUrl = url.split('='); 
    this.videoUrl =  this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + youtubeUrl[1]);
    this.videoAvailable=false;
    }
    else{
      this.videoAvailable=true;
    }
  }
  
   logout() {
    this.authProvider.logout();
  }

  onGetGynecologicalWeekInformation = (callback) =>{
    callback();
    return response => {
    this.getSafeUrl(response.video);
    this.examinationInfo = response;
    }
  }

  getGynecologicalWeekInformation(week){
    let loading = this.loading.create({
      spinner: 'crescent',
      content: 'Παρακαλώ περιμένετε ...'
    });
    loading.present();
    this.examinationResultService.getExaminationResult(week,this.onGetGynecologicalWeekInformation(() => {
      loading.dismiss();
    }),null);

}

}