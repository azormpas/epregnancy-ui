import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExaminationResult } from './examination-result';



@NgModule({
  declarations: [ExaminationResult],
  imports: [
   IonicPageModule.forChild(ExaminationResult),
  // TranslateModule.forChild()
  ]
})

export class ExaminationResultModule { }