import { Component,ViewChild } from '@angular/core';
import { NavController, IonicPage,Tabs } from 'ionic-angular';

import { ProfilePage } from './../profile/profile';
import { HomePage } from './../home/home';

@IonicPage({
  name: "tabs",
  segment: "tabs"
 // defaultHistory: ["home", "profile"]
})


@Component({
  selector:'tabs-page',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root:any = 'home';
  tab2Root:any = 'profile';
  tab3Root:any = 'weight-chart';
  tab4Root:any = 'forum-main-page';

  constructor(private navCtrl : NavController) {


  }



 

  
}


