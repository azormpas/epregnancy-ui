import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Tabs } from 'ionic-angular';
import { Storage, IonicStorageModule } from "@ionic/storage";
import { JwtHelper, AuthConfig, AuthHttp } from "angular2-jwt";
import { Http, HttpModule, RequestOptions } from "@angular/http";

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { WelcomePage } from './../pages/welcome/welcome';
import { RegisterPage } from '../pages/register/register';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { TabsPage } from '../pages/tabs/tabs';
import { ExaminationResult } from "../pages/examination-result/examination-result";
import { TestList } from '../pages/testList/testList';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AuthProvider } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { RegistrationService } from '../services/registration.service';
import { ProfileService } from '../services/profile.service';

import { RestClient } from '../services/rest-client.service';
import { ErrorUtil } from '../services/error.util';
import { SuccessUtil } from '../services/success.util';
import { HomeService } from '../services/home.service';
import { TestListService } from '../services/testList.service';
import { ExaminationResultService } from '../services/examinationResult.service';
import { WeightChartService } from '../services/weight-chart.service';

export function authHttpServiceFactory(http: Http, options: RequestOptions, storage: Storage) {
  const authConfig = new AuthConfig({
    tokenGetter: (() => storage.get('jwt')),
  });
  return new AuthHttp(authConfig, http, options);
  
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md',
      scrollAssist: false,
      autoFocusAssist: false,
      monthNames: ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος",
      "Μαϊος", "Ιούνιος", "Ιούλιος", "Αύγουστος",
      "Σεπτέμβριος", "Οκτώμβριος", "Νοέμβριος", "Δεκέβριος" ]
    
    }, ),
    IonicStorageModule.forRoot({
      name: 'myapp',
      driverOrder: ['sqlite', 'indexeddb', 'websql']
    }),],
  bootstrap: [IonicApp],
  exports: [
    MyApp
  ],

  providers: [
    StatusBar,
    SplashScreen,
    AuthProvider,
    UserService,
    ErrorUtil,
    SuccessUtil,
    RestClient,
    ProfileService,
    TestListService,
    RegistrationService,
    ExaminationResultService,
    WeightChartService,
    HomeService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    JwtHelper, {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions, Storage]
    }
  ]
})
export class AppModule { }
