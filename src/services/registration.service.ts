import { Injectable } from '@angular/core';
import { RestClient } from './rest-client.service';
import { Headers } from '@angular/http';
import { environment } from '../enviroments/environment.prod';
import {Storage} from "@ionic/storage";


@Injectable()
export class RegistrationService {

 
    constructor(public restClient: RestClient,private storage: Storage) {
    }

    //   var headers:Headers = new Headers({
    //     'Content-Type': 'application/json',
    //     'Authorization': this.storage.getItem('jwt')
    //   });
    registration(data, successCallback, errorCallback): void {
        this.restClient.postWithoutAuth(environment.CONSTANTS.API_ROOT + '/user/signup', null, data, successCallback, errorCallback).subscribe();
    };


}
