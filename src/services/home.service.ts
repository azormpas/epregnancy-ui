import { Injectable } from '@angular/core';
import { RestClient } from './rest-client.service';
import { Headers } from '@angular/http';
import { environment } from '../enviroments/environment.prod';
import { Storage } from "@ionic/storage";
import { AuthHttp } from 'angular2-jwt';
import { ReplaySubject } from 'rxjs';


@Injectable()
export class HomeService {

    

    constructor(public restClient: RestClient) {
    }


    checkDate(successCallback, errorCallback): void {
        this.restClient.get(environment.CONSTANTS.API_ROOT + '/calendar/date', successCallback, errorCallback).subscribe();
    };

    saveConceptualDate(data, successCallback, errorCallback): void {
        this.restClient.post(environment.CONSTANTS.API_ROOT + '/calendar/save/date', null, data, successCallback, errorCallback).subscribe();
    };

    getValidConceptualDate(successCallback, errorCallback): void {
        this.restClient.get(environment.CONSTANTS.API_ROOT + '/calendar/dropdownDate',successCallback, errorCallback).subscribe();
    };


}