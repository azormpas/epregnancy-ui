import { Injectable } from '@angular/core';
import { RestClient } from './rest-client.service';
import { Headers } from '@angular/http';
import { environment } from '../enviroments/environment.prod';
import { Storage } from "@ionic/storage";
import { AuthHttp } from 'angular2-jwt';
import { ReplaySubject } from 'rxjs';


@Injectable()
export class WeightChartService {

    
    constructor(public restClient: RestClient) {
    }

    getWeekList(successCallback, errorCallback): void {
        this.restClient.get(environment.CONSTANTS.API_ROOT + '/week/list', successCallback, errorCallback).subscribe();
    };

    saveUserBMI(data,successCallback, errorCallback): void {
        this.restClient.post(environment.CONSTANTS.API_ROOT + '/chart/save/bmi', null, data,successCallback, errorCallback).subscribe();
    };

    getUserBMI(successCallback, errorCallback): void {
        this.restClient.get(environment.CONSTANTS.API_ROOT + '/chart/get/bmi', successCallback, errorCallback).subscribe();
    };


}