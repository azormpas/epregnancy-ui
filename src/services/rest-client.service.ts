import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions,  Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { LoadingController } from 'ionic-angular';
import { ErrorUtil } from './error.util';
import { SuccessUtil } from './success.util';
import { RequestOptionsArgs } from '@angular/http/src/interfaces';
import { AuthHttp } from 'angular2-jwt';


@Injectable()
export class RestClient {

    constructor( private errorUtil: ErrorUtil, private successUtil: SuccessUtil,
         private loading: LoadingController,private readonly authHttp: AuthHttp,private readonly http:Http) {
    }

    get(url: string, successCallback: Function, errorCallback: Function): Observable<any> {

        let  headers = new Headers({
            'Content-Type': 'application/json'
        })

        return this.authHttp.get(url, {headers})
            .map((response: Response) => {
                console.log(response)
                if (response) {
                    if (successCallback) {
                        successCallback(response.json());
                    }
                    else {
                        this.successUtil.showMessage(response.json().code);
                    }
                    return response.json();
                }
            })
            .catch((error: any) => {
                // console.log(error);
                var errorObject = JSON.parse(error['_body']);
                if (errorCallback) {
                    //errorCallback(error);
                    errorCallback(errorObject);
                }
                else {
                    this.errorUtil.showError(errorObject.code);
                }
                // throw Observable.throw(error['_body']);
                return  Observable.of(errorObject);
            });
    }

    post(url: string, h: Headers, data: Object, successCallback: Function, errorCallback: Function): Observable<any> {

        let loading = this.loading.create({
            spinner: 'crescent',
            content: 'Παρακαλώ περιμένετε ...'
          });
      
        
        var cpheaders;
        
        if (h != null) {
            cpheaders = h;
        }
        else {
            cpheaders = new Headers({ 'Content-Type': 'application/json' });
        }

        var requestOptions = new RequestOptions({
            headers: cpheaders
        });
        return this.authHttp.post(url, data, requestOptions)
            .map((response: Response) => {
                if (response) {
                    if (successCallback) {
                        successCallback(response.json());
                    }
                    else {         
                        this.successUtil.showMessage(response.json().code);
                    }
                    loading.dismiss();
                    return response;
                }
            })
            .catch((error: any) => {
                var errorObject = JSON.parse(error['_body']);
                if (errorCallback) {
                   // errorCallback(error);
                   errorCallback(errorObject);
                }
                else {
                    this.errorUtil.showError(errorObject.code);
                }
                loading.dismiss();
                return  Observable.of(errorObject);
                            
            });

    }



    postWithoutAuth(url: string, h: Headers, data: Object, successCallback: Function, errorCallback: Function): Observable<any> {

        let loading = this.loading.create({
            spinner: 'crescent',
            content: 'Παρακαλώ περιμένετε ...'
          });
      
        
        var cpheaders;
        
        if (h != null) {
            cpheaders = h;
        }
        else {
            cpheaders = new Headers({ 'Content-Type': 'application/json' });
        }

        var requestOptions = new RequestOptions({
            headers: cpheaders
        });
        return this.http.post(url, data, requestOptions)
            .map((response: Response) => {
                if (response) {
                    if (successCallback) {
                        successCallback(response.json());
                    }
                    else {         
                        this.successUtil.showMessage(response.json().code);
                    }
                    loading.dismiss();
                    return response;
                }
            })
            .catch((error: any) => {
                var errorObject = JSON.parse(error['_body']);
                if (errorCallback) {
                    errorCallback(error);
                }
                else {
                    this.errorUtil.showError(errorObject.code);
                }
                loading.dismiss();
                return  Observable.of(errorObject);
                            
            });

    }
    
}