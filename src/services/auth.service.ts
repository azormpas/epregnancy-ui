import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
import {ReplaySubject, Observable} from "rxjs";
import {Storage} from "@ionic/storage";
import {JwtHelper, AuthHttp} from "angular2-jwt";
import { environment } from '../enviroments/environment.prod';

@Injectable()
export class AuthProvider {

  authUser = new ReplaySubject<any>(1);

  constructor(private readonly http: Http,
              private readonly authHttp: AuthHttp,
              private readonly storage: Storage,
              private readonly jwtHelper: JwtHelper) {}

  checkLogin() {
    this.storage.get('jwt').then(jwt => {
   
      if (jwt && !this.jwtHelper.isTokenExpired(jwt)) {
        this.authHttp.get(`${environment.CONSTANTS.API_ROOT}/authenticate`)
          .subscribe(() => this.authUser.next(jwt),
            (err) => this.storage.remove('jwt').then(() => this.authUser.next(null)));
      }
      else {
        this.storage.remove('jwt').then(() => this.authUser.next(null));
      }
    });
  }

  login(values: any): Observable<any> {
    return this.http.post(`${environment.CONSTANTS.API_ROOT}/authentication/login`, values)
      .map(response => response.text())
      .map(jwt =>{
        this.handleJwtResponse(jwt)
        // localStorage.setItem("token",jwt);
        // console.log("HELLOSSS"); 
        // console.log(jwt)   
      } );
  }

  logout() {
    this.storage.remove('jwt').then(() => this.authUser.next(null));
  }

  private handleJwtResponse(jwt: string) {
    return this.storage.set('jwt', jwt)
      .then(() => this.authUser.next(jwt))
      .then(() => jwt);
  }

}