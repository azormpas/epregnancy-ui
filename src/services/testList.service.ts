import { Injectable } from '@angular/core';
import { RestClient } from './rest-client.service';
import { Headers } from '@angular/http';
import { environment } from '../enviroments/environment.prod';
import { Storage } from "@ionic/storage";
import { AuthHttp } from 'angular2-jwt';


@Injectable()
export class TestListService {

    

    constructor(public restClient: RestClient) {
    }


    getGynecologicalTestList(param,successCallback, errorCallback): void {
        this.restClient.get(environment.CONSTANTS.API_ROOT + '/gynecologicalTest/testList/' + `${param}`, successCallback, errorCallback).subscribe();
    };

    saveConceptualDate(data, successCallback, errorCallback): void {
        this.restClient.post(environment.CONSTANTS.API_ROOT + '/calendar/save/date', null, data, successCallback, errorCallback).subscribe();
    };


}