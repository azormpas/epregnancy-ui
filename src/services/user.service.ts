import { JwtHelper } from 'angular2-jwt';
import { AuthProvider } from './auth.service';
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import { User } from '../model/user';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

  private cpheaders = new Headers({ 'Content-Type': 'application/json' });

 private userUrl = "http://localhost:8080/iBabyApp/user";
 private gynecologicalTestControllerUrl = "http://localhost:8080/iBabyApp/gynecologicalTest";
 //private userUrl = "http://ebabycoming.com/iBabyApp/user";
 //private gynecologicalTestControllerUrl = "http://ebabycoming.com/iBabyApp/gynecologicalTest";

  // URL to web api

  private token: string;

  private headers;

  private email:string;


  constructor(private http: Http, private authProvider: AuthProvider, private authHttp: AuthHttp,public jwtHelper: JwtHelper) {

     this.authProvider.authUser.subscribe(jwt => {
      if (jwt) {
        this.token = jwt;
        const decoded = this.jwtHelper.decodeToken(jwt);
        this.email = decoded.sub;
       
      }
      else {
        this.email = null;
      }
    });
  

    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.token
    });
  }

  create(user: User, controllerUrl): Promise<any> {
    let url = this.userUrl + controllerUrl;
    let options = new RequestOptions({ headers: this.cpheaders });
    return this.http
      .post(url, user, options)
      .toPromise()
      .then(data => data.status)
      .catch(err => err.status);
  }

    update(user: User,controllerUrl): Promise<any> {
    let url = `${this.userUrl}/${controllerUrl}`;
    return this.authHttp
      .post(url, user)
      .toPromise()
      .then(data => data.status)
      .catch(err => err.status);
  }


  getUserData(controllerUrl: string): Promise<any> {
    let url = `${this.userUrl}/${controllerUrl}`;
    return this.authHttp
      .post(url, this.email)
      .toPromise()
      .then(data => data.json())
      .catch(err => err.status);
  }

  save(controllerUrl: string, data: any): Promise<any> {
    let url = `${this.userUrl}/${controllerUrl}`;

    return this.authHttp
      .post(url, data)
      .toPromise()
      .then(data => data.json())
      .catch(err => err.status);
  }


  //get the testList 
    getGynecologicalTest(controllerUrl: string, data: any): Promise<any> {
    let url = `${this.gynecologicalTestControllerUrl}/${controllerUrl}`;
    return this.authHttp
      .post(url, data)
      .toPromise()
      .then(data => data.json())
      .catch(err => err.status);
  }
}


