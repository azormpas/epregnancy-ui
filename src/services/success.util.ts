import { Injectable } from '@angular/core';
import * as successMessages from '../app/_resources/success-messages.json';
import { ToastController } from "ionic-angular"; 
//import { TranslationService } from './translation.service';


@Injectable()
export class SuccessUtil {
    

    constructor(private readonly toastCtrl: ToastController) {
    }


    showMessage(messageCode){

        let  successMessage;
      //  TranslationService.translate(successMessages[messageCode]).subscribe(translated =>{ 
         //   successMessage = translated;
            const toast = this.toastCtrl.create( {
                message: successMessages[messageCode],
                duration: 4000,
                position: "top"
              });
    
            toast.present(); 
      //  });

    }

}



