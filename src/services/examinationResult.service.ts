import { Injectable } from '@angular/core';
import { RestClient } from './rest-client.service';
import { Headers } from '@angular/http';
import { environment } from '../enviroments/environment.prod';
import { Storage } from "@ionic/storage";
import { AuthHttp } from 'angular2-jwt';
import { ReplaySubject } from 'rxjs';


@Injectable()
export class ExaminationResultService {

    

    constructor(public restClient: RestClient) {
    }

    getExaminationResult(param,successCallback, errorCallback): void {
        this.restClient.get(environment.CONSTANTS.API_ROOT + '/gynecologicalTest/weeklyExamination/'+ `${param}`,successCallback, errorCallback).subscribe();
    };


}