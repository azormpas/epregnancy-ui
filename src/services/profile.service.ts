import { Injectable } from '@angular/core';
import { RestClient } from './rest-client.service';
import { Headers } from '@angular/http';
import { environment } from '../enviroments/environment.prod';
import { Storage } from "@ionic/storage";


@Injectable()
export class ProfileService {


    constructor(public restClient: RestClient, private storage: Storage) {
    }


    getUserData(data, successCallback, errorCallback): void {
        this.restClient.post(environment.CONSTANTS.API_ROOT + '/user/loadProfileData', null, data, successCallback, errorCallback).subscribe();
    };

    updateUserData(data, successCallback, errorCallback): void {
        this.restClient.post(environment.CONSTANTS.API_ROOT + '/user/updateProfileData', null, data, successCallback, errorCallback).subscribe();
    };

    getValidConceptualDate(successCallback, errorCallback): void {

        this.restClient.get(environment.CONSTANTS.API_ROOT + '/calendar/dropdownDate', successCallback, errorCallback).subscribe();
    };


}
