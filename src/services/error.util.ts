import { Injectable } from '@angular/core';
import * as errorMessages from '../app/_resources/error-messages.json'
import { ToastController } from "ionic-angular";
//import { TranslationService } from './translation.service';


@Injectable()
export class ErrorUtil {

    constructor(private readonly toastCtrl: ToastController) {
        console.log("IMPORT OR NO");

        console.log(errorMessages);
    }


    showError(errorCode) {
    //  let  errorMessage;
    //  TranslationService.translate(errorMessages[errorCode]).subscribe(translated =>{ 
    //   errorMessage = translated
 
             const toast = this.toastCtrl.create({
                message: errorMessages[errorCode],
                duration: 4000,
                position: "top"
            });
            toast.present();
      //  });

    }

}



