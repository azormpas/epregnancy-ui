/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare module "*.json" {
  const json: any;
  export default json;
}

// declare module "*.enviroment.dev.ts" {
//   const value: any;
//   export default value;
// }

declare module "*.enviroment.prod" {
  const value: any;
  export default value;
}


